import React from 'react';
import logo from './logo.svg';
import './App.css';
import { connect } from 'react-redux';

function App(props) {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <h1>
          {`Count is ${props.count}`}
        </h1>
      </header>
    </div>
  );
}

const MapStateToProps = (state) => {
    return state;
}

export default connect(MapStateToProps)(App);
